package com.example.firebaseauthentication

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        btnLogOff.setOnClickListener(this)
        }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogOff ->{
                var fbAuth = FirebaseAuth.getInstance()

                fbAuth.signOut()
                var intent = Intent (this, MainActivity::class.java)
                startActivity(intent)
            }
        }
    }
}