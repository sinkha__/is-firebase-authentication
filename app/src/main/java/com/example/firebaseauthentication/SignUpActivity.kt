package com.example.firebaseauthentication

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity(), View.OnClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btnReg.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var email = edRegUserName.text.toString()
        var password = edRegPass.text.toString()

        if(email.isEmpty() || password.isEmpty()){
            Toast.makeText(this, "username/password cannot be empty", Toast.LENGTH_LONG).show()
        }
        else {
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("registering ...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this, "Successfully Register..", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this, "Username/password incorrect", Toast.LENGTH_SHORT).show()
                }
        }
    }
}